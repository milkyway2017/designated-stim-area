#include "WindowFrame.h"
#include <vector>
class SettingsFrame : public WindowFrame {
private:
	/*cegui callbacks *******************************/
	//handles the event fired by the exit button
	bool ExitFrame(const CEGUI::EventArgs& e);
	
	//handles when the resolutionBox's accepted value changed
	bool ResolutionChanged(const CEGUI::EventArgs& e);
	/************************************************/
	Widget_ptr* ConsolePtr;
	CEGUI::Combobox* resolutionBox;
public:
	SettingsFrame(Logger* log);
	~SettingsFrame();
protected:
	std::vector<Command> CommandProc(std::vector<Command> commmands);
	virtual std::vector<Command> Update(Uint32 ElapsedTime);
	virtual void Draw();
	virtual bool IsCapturingInput();
	
};