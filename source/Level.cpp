#include "Level.h"
#include <nlohmann/json.hpp>

using namespace Fengine;
Level::Level(std::string path, bool IsPrimary, std::string LevelName, Logger* systemlog) :
	Visible(0),
	Updatable(0),
	IsPrimaryLevel(IsPrimary),
	ShouldBeDeleted(0),
	LevelName(LevelName),
	RespondToInput(1),
	SystemLog(systemlog),
	LevelCore(new Core())
{
	Graphics::GetInstance()->GenShaderProgram("Resources/Shaders/Normal Rendering/white.vert", "Resources/Shaders/Normal Rendering/white.frag", ShaderProgram, "Normal Rendering");
	camera.Init(ShaderProgram, "Matrix");
	SDL_Point ScreenWH = Graphics::GetInstance()->GetWindowSize();
	camera.SetPosition(glm::vec2((ScreenWH.x / 2), ScreenWH.y / 2));		//set the camera to the middle of the screen
	std::ifstream level(path.c_str());

	using json = nlohmann::json;

	json levelJSON;

	level >> levelJSON;
	levelJSON = levelJSON["level"];

	SDL_Rect ViewP{ -1, -1, -1, -1 };

	try
	{
		ViewP.x = levelJSON["x"];
		ViewP.y = levelJSON["y"];
		ViewP.w = levelJSON["w"];
		ViewP.h = levelJSON["h"];
	}
	catch (...)
	{
		//I have to put here something
		throw Exception({ "Level Loading Error",	"failed to load viewport" }, true);
	}

	this->Viewport = ViewP;

	//check if the level contains backgroundlist
	if (levelJSON.find("backgroundlist") != levelJSON.end())
	{
		json backgroundList = levelJSON["backgroundlist"];
		//check if the background list is empty
		if (!backgroundList.empty())
		{
			try
			{
				for (auto bImage : backgroundList)
				{
					Sprite image;
					SDL_Rect destRect;

					destRect.x = bImage["x"];
					destRect.y = bImage["y"];
					destRect.w = bImage["w"];
					destRect.h = bImage["h"];

					image.Init(bImage["imagepath"], Rect4F(destRect));			//Loads the image

					LevelCore->AddBackgroundImage(image);				 //and store it to list
				}
			}
			catch (const std::exception & e)
			{
				//I have to put here something
				throw Exception({ "Level Loading Error","failed to load background image", e.what() }, true);
			}
		}
	}

	//load Game World if there is any
	if (levelJSON.find("gameworld") != levelJSON.end())
	{
		try
		{
			std::string gpath = levelJSON["gameworld"][0]["path"];  //How to convert json to char*
			LevelCore->SetGameWorld(new GameWorld(gpath, systemlog, ShaderProgram));
		}
		catch (const Exception& e)
		{
			//if the Gameworld fails to load, set GameWorld to nullptr and pass the e exception
			LevelCore->SetGameWorld(nullptr);
			throw e;
		}
	}
}


Level::~Level()
{
}
std::vector<Command> Level::Update(int TimeElapsed, Input & input)
{
	if (Updatable)
	{
		//update key input and Buttons
		std::vector<Command> CommandList = KeyInputHandler(input);
		CommandQueue.insert(CommandQueue.begin(), CommandList.begin(), CommandList.end());

		//update gameworlds
		if (LevelCore->HasGameWorld())		//if this level has gameworld
		{
			if (LevelCore->world->Active)
			{
				if (RespondToInput)
					CommandList = LevelCore->world->Update(TimeElapsed, input);
				else
				{
					Input blankinput;
					CommandList = LevelCore->world->Update(TimeElapsed, blankinput);
				}
				CommandQueue.insert(CommandQueue.begin(), CommandList.begin(), CommandList.end());
			}
		}
		return CommandProc();
	}
	return std::vector<Command>();
}

void Level::Draw()
{
	if (Visible)
	{
		//set viewport to
		glViewport(Viewport.x, Viewport.y, Viewport.w, Viewport.h);
		if (LevelCore->HasGameWorld())						//if this level has gameworld
		{
			LevelCore->world->Draw();
		}
		for (unsigned int i = 0; i < LevelCore->BackgroundList.size(); i++)			//draw all backgrounds if there is any
		{
			LevelCore->BackgroundList[i].Draw(ShaderProgram);
		}
		//set viewport back to
		glViewport(0, Graphics::GetInstance()->GetWindowSize().x, Graphics::GetInstance()->GetWindowSize().y, 0);

		//Graphics::GetInstance()->UnbindAll();
	}
}

void Level::ActivateLevel()
{
	this->Visible = 1;
	this->Updatable = 1;
}

void Level::DeactivateLevel()
{
	this->Visible = 0;
	this->Updatable = 0;
}

std::vector<Command> Level::CommandProc()
{
	COMMANDPROCBEGIN
		int commandLevel = i.command_level_;
	// now we have everything we need to process this command
	if (commandLevel < LEVEL)
	{
		ReturnList.push_back(i);
		continue;
	}
	else if (commandLevel > LEVEL)
	{
		if (LevelCore->HasGameWorld())
		{
			this->LevelCore->world->AddCommand(i);
			continue;
		}
	}
	else
	{
		switch ((LevelCommandType)i.command_type_)
		{
		case LevelCommandType::DELLVL:
			this->ShouldBeDeleted = true;					//set not active
			i.Resolve();			//resolve
			break;
		case LevelCommandType::NEW_RES:
			camera.NewRes();
			break;
		default:
			i.CannotResolve(SystemLog);
			continue;
		}
	}
		COMMANDPROCEND
}

std::vector<Command> Level::KeyInputHandler(Input & input)
{
	return std::vector<Command>();
}

void Core::AddBackgroundImage(Sprite s)
{
	BackgroundList.push_back(s);
}

void Core::SetGameWorld(GameWorld * gw)
{
	world.reset(gw);
}

bool Core::HasGameWorld()
{
	return world.get();
}
