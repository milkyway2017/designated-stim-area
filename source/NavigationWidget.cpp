#include "NavigationWidget.h"



bool NavigationWidget::ExitToMenuClicked(const CEGUI::EventArgs & e)
{
	CommandQueue.push_back(Command(PROGRAM,int(ProgramCommandType::CGF),"NavigationWidget", "(0)"));
	CommandQueue.push_back(Command(FRAME,int(FrameCommandType::TOGGLE_SHADOW), "NavigationWidget","(0)"));
	Root->setVisible(0);
	return true;
}

bool NavigationWidget::GameSettingsClicked(const CEGUI::EventArgs & e)
{
	CommandQueue.push_back(Command(PROGRAM, int(ProgramCommandType::CGF), "NavigationWidget", "(2)"));
	Root->setVisible(0);
	CommandQueue.push_back(Command(FRAME, int(FrameCommandType::TOGGLE_SHADOW), "NavigationWidget","(1)"));
	return true;
}

bool NavigationWidget::ReturnToGameClicked(const CEGUI::EventArgs & e)
{
	Root->setVisible(0);
	CommandQueue.push_back(Command(FRAME,int(FrameCommandType::TOGGLE_SHADOW)/*"[frame]<toggleshadow>(0)"*/, "NavigationWidget", "(0)"));
	return true;
}

std::string NavigationWidget::GetType()
{
	return "NavigationWidget";
}

NavigationWidget::NavigationWidget():
	IntegratedWidget("/IntegratedWidget/Navigation.layout")
{
	ExitToMenu = static_cast<CEGUI::PushButton*>(Root->getChild("ExitToMenu"));
	ReturnToGame = static_cast<CEGUI::PushButton*>(Root->getChild("ReturnToGame"));
	GameSettings = static_cast<CEGUI::PushButton*>(Root->getChild("GameSettings"));

	ExitToMenu->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&NavigationWidget::ExitToMenuClicked, this));
	ReturnToGame->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&NavigationWidget::ReturnToGameClicked, this));
	GameSettings->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&NavigationWidget::GameSettingsClicked, this));
}


NavigationWidget::~NavigationWidget()
{
}