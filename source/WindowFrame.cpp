#include "WindowFrame.h"


void WindowFrame::AddNavigationWidget()
{
	if (GetStaticWidget("NavigationWidget") == nullptr)
	{
		WidgetList.push_back(Widget_ptr(new NavigationWidget()));
		//add the widget to shadow
		WidgetList.back()->AttachToWindow(this->Shadow);
		Root->addChild(this->Shadow);
	}
}

bool WindowFrame::IsCapturingInput()
{
	bool IsCapturing = false;

	//first loop through all widgets
	for (auto& i : WidgetList)
	{
		if (i->IsCapturingInput())
		{
			IsCapturing = 1;
			break;
		}
	}

	if (Shadow->isVisible())
		IsCapturing = 1;

	return IsCapturing;
}

bool WindowFrame::ToggleNavigationWidget(bool onoff)
{	
	GetStaticWidget("NavigationWidget")->Toggle(onoff);
	return false;
}

bool WindowFrame::OpenNavigation(const CEGUI::EventArgs & e)
{
	ToggleNavigationWidget(1);
	return true;
}

void WindowFrame::AddConsoleWidget(bool withShadow)
{
	if (GetStaticWidget("ConsoleWidget") != nullptr)
	{
		return;
	}
	WidgetList.push_back(Widget_ptr(new ConsoleWidget()));
	if (withShadow)
	{
		//if wanted to invoke with shadow
		//attach to shadow
		//add shadow to root
		WidgetList.back()->AttachToWindow(this->Shadow);
		Root->addChild(this->Shadow);
	}
	else
	{
		WidgetList.back()->AttachToWindow(this->Root);
	}
	WidgetList.back()->LinkValueToAttribute("Logger", SystemLog);
}

void WindowFrame::ToggleConsoleWidget(bool onoff)
{
	auto i = GetStaticWidget("ConsoleWidget");
	i->Toggle(onoff);
	i->Root->activate();
}

unsigned int WindowFrame::AddMessageBoxWidget(const std::string & title, const std::string & body, Functor f)
{
	WidgetList.push_back(Widget_ptr(new MessageBoxWidget(f, title, body)));
	MessageBoxWidget* newWidget = (MessageBoxWidget*)WidgetList.back().get();
	unsigned int widgetID = newWidget->GetID();

	//attach to shadow if is fatal
	//else attach to root
	if (newWidget->IsFatal)
	{
		newWidget->AttachToWindow(Shadow);
	}
	else
	{
		newWidget->AttachToWindow(Root);
	}
	ToggleMessageBoxWidget(widgetID, true);
	return widgetID;
}

unsigned int WindowFrame::AddMessageBoxWidget(const Exception & e, Functor f)
{
	WidgetList.push_back(Widget_ptr(new MessageBoxWidget(e,f)));
	MessageBoxWidget* newWidget = (MessageBoxWidget*)WidgetList.back().get();
	unsigned int widgetID = newWidget->GetID();

	//attach to shadow if is fatal
	//else attach to root
	if (newWidget->IsFatal)
	{
		newWidget->AttachToWindow(Shadow);
	}
	else
	{
		newWidget->AttachToWindow(Root);
	}
	ToggleMessageBoxWidget(widgetID, true);
	return widgetID;
}

void WindowFrame::ToggleMessageBoxWidget(unsigned int widgetID, bool onoff)
{
	MessageBoxWidget* msgWidget = (MessageBoxWidget*)GetWidget(widgetID);
	if (msgWidget->Parent == this->Shadow)
	{
		//if attached, the shadow should also be toggled
		ToggleShadow(onoff);
	}
	msgWidget->Toggle(onoff);
}

void WindowFrame::AddConfirmExitWidget(const std::string & Title, const std::string & Message, 
	const std::string & Opt1,
	const std::string & Opt2,
	Functor callback)
{
	if (GetStaticWidget("PromptWidget") != nullptr)
	{
		return;
	}
	if(callback.is_default_)
		WidgetList.push_back(Widget_ptr(new PromptWidget(Functor(this, &WindowFrame::HandleConfirmExit), Title, Message, Opt1, Opt2)));
	else
		WidgetList.push_back(Widget_ptr(new PromptWidget(callback, Title, Message, Opt1, Opt2)));
	WidgetList.back()->AttachToWindow(this->Shadow);
	Root->addChild(this->Shadow);
}

void WindowFrame::ToggleConfirmExitWidget(bool onoff)
{
	auto i = GetStaticWidget("PromptWidget");
	i->Toggle(onoff);
}

bool WindowFrame::OpenConfirmExit(const CEGUI::EventArgs & e)
{
	ToggleConfirmExitWidget(1);
	return true;
}

void WindowFrame::ToggleShadow(bool onoff)
{
	if (onoff)
	{
		Shadow->setVisible(1);
		Shadow->activate();
	}
	else
	{
		Shadow->setVisible(0);
	}

}

std::vector<Command> WindowFrame::CommandProc()
{
	COMMANDPROCBEGIN
		int commandLevel = i.command_level_;
		if (commandLevel < FRAME)
		{
			ReturnList.push_back(i);
			continue;
		}
		else if (commandLevel > FRAME)
		{
			this->level->AddCommand(i);
			continue;
		}
		try
		{
			auto paramStringList = i.param_string_list_;
			switch ((FrameCommandType)i.command_type_)
			{
			case FrameCommandType::LOAD_LEVEL:
					if (IsLevelValid())
					{
						level.reset();
					}
					level.reset(new Level(paramStringList[0], 1, paramStringList[1], this->SystemLog));
					level->ActivateLevel();
					i.Resolve();			//resolve
					break;
			case FrameCommandType::TOGGLE_CONSOLE:
					ToggleConsoleWidget(std::stoi(paramStringList[0]));
					i.Resolve();			//resolve
					break;
			case FrameCommandType::TOGGLE_SHADOW:
					ToggleShadow(std::stoi(paramStringList[0]));
					i.Resolve();			//resolve
					break;
			case FrameCommandType::TOGGLE_WIDGET:
			{
				auto *widget = GetWidget(std::stoi(paramStringList[0]));
				widget->Toggle(std::stoi(paramStringList[1]));
			}
					i.Resolve();
					break;
			case FrameCommandType::ADD_WIDGET:
					if (i.param_string_list_[0] == "PromptWidget")
						AddConfirmExitWidget(i.param_string_list_[1]/*title*/, i.param_string_list_[2]/*prompt*/,
							i.param_string_list_[3]/*string of option 1*/, i.param_string_list_[4]/*string of option 2*/,
							*(Functor*)i.target_
						);
					else if (i.param_string_list_[0] == "MessageBoxWidget")
						AddMessageBoxWidget(i.param_string_list_[1], i.param_string_list_[2], *(Functor*)i.target_);

					i.Resolve();
					break;
			default:
					break;
			}
		}
		catch (const Exception & e)
		{
			//cannot resolve because an exception is thrown
			i.CannotResolve(SystemLog);
			throw e;
		}

	COMMANDPROCEND
}

std::vector<Command> WindowFrame::KeyInputHandler(Input & input)
{
	std::vector<Command> ReturnList;

	if (input.IsKeyPressed(SDL_SCANCODE_GRAVE))
	{
			auto i = GetStaticWidget("ConsoleWidget");
			if (!i)
			{
				throw Exception("Cannot toggle console", false);
			}
			else
			{
				ToggleConsoleWidget(!i->Root->isVisible());
			}
	}

	return ReturnList;
}

IntegratedWidget * WindowFrame::GetWidget(unsigned int WidgetID)
{
	for (auto &i : WidgetList)
	{
		if (i->GetID() == WidgetID)
		{
			return i.get();
		}
	}
	//error message
	throw Exception({ "Cannot find widget with given ID!",
		"line: " + std::to_string(__LINE__),
		"file: " + std::string(__FILE__) },
		false);
}

void WindowFrame::RemoveWidget(unsigned int WidgetID)
{
	for (auto i = WidgetList.begin(); i != WidgetList.end();i++)
	{
		if ((*i)->GetID() == WidgetID)
		{
			WidgetList.erase(i);
			return;
		}
	}
	//error message
	throw Exception({ "Cannot find the widget with ID " +std::to_string(WidgetID)+ " to remove!",
		"line: "+std::to_string(__LINE__),
		"file: "+std::string(__FILE__)},
		false);
}

void WindowFrame::RemoveStaticWidget(const std::string & Type)
{
	for (auto i = WidgetList.begin(); i != WidgetList.end(); i++)
	{
		if ((*i)->GetType() == Type)
		{
			WidgetList.erase(i);
			return;
		}
	}
	//error message
	throw Exception({ "Cannot find the "+Type+" to remove!",
		"line: " + std::to_string(__LINE__),
		"file: " + std::string(__FILE__) },
		false);
}

WindowFrame::WindowFrame(WindowFrameType type, Logger* _log) :
	Type(type),
	UIContext(&(GUI::GetGUIClass()->GetGUIContext())),
	SystemLog(_log)
{
	Shadow = GUI::GetGUIClass()->LoadWindow("/IntegratedWidget/Shadow.layout");
	ToggleShadow(0);
}

void WindowFrame::HandleExceptionOK(void * WidgetID)
{
	unsigned int id = *(unsigned int*)WidgetID;

	if (((MessageBoxWidget*)GetWidget(id))->IsFatal)
	{	//toggle off shadow if shadow was on because of the fatality of the message
		ToggleShadow(0);
	}

	RemoveWidget(id);
}

void WindowFrame::HandleCopyNPaste()
{
	//if control is held
	if (input.IsKeyHeld(SDL_SCANCODE_LCTRL) || input.IsKeyHeld(SDL_SCANCODE_RCTRL))
	{
		if (input.IsKeyPressed(SDL_SCANCODE_C))
		{
			UIContext->injectCopyRequest();
		}
		else if (input.IsKeyPressed(SDL_SCANCODE_V))
		{
			UIContext->injectPasteRequest();
			CEGUI::SDLClipboardProvider::GetInstance()->Deallocate();
		}
		else if (input.IsKeyPressed(SDL_SCANCODE_X))
		{
			UIContext->injectCutRequest();
		}
	}
}

std::vector<Command> WindowFrame::Update(Uint32 ElapsedTime)
{
	try
	{
		//Update the ConsoleWidge
		auto r = GetStaticWidget("ConsoleWidget");
		if(r)
			r->Update();

		CEGUI::System::getSingleton().injectTimePulse(ElapsedTime / 1000.f);
		UIContext->injectTimePulse(ElapsedTime / 1000.f);

		HandleCopyNPaste();

		std::vector<Command> TempList = std::vector<Command>();

		if (IsLevelValid())
		{
			TempList = level->Update(ElapsedTime, input);
		}

		if (IsLevelValid())
		{
			//if the level is not valid, delete it, just for fun!
			if (level->GetShouldBeDeleted())
			{
				level.reset();
			}
		}

		//add to command queue
		this->CommandQueue.insert(CommandQueue.end(), TempList.begin(), TempList.end());

		TempList = KeyInputHandler(input);
		//add to command queue
		this->CommandQueue.insert(CommandQueue.end(), TempList.begin(), TempList.end());

		//add widgets' commands to command queue
		for (auto &i : WidgetList)
		{
			TempList = i->ExtractCommand();
			this->CommandQueue.insert(CommandQueue.end(), TempList.begin(), TempList.end());
		}

		//process command queue
		return CommandProc();
	}
	catch (const Exception & e)
	{
		CommandQueue.clear();
		//see if already exists a MessageBox
		//issue here, find a way to spawn more than one messagebox!
		unsigned int ID = AddMessageBoxWidget(e, Functor(this, &WindowFrame::HandleExceptionOK));
		ToggleMessageBoxWidget(ID, true);
		return std::vector<Command>();
	}
	catch (const std::exception & e)
	{
		CommandQueue.clear();
		//see if already exists a MessageBox
		//issue here, find a way to spawn more than one messagebox!
		unsigned int ID = AddMessageBoxWidget("C++ std exception throw", e.what(), Functor(this, &WindowFrame::HandleExceptionOK));
		ToggleMessageBoxWidget(ID, true);
		return std::vector<Command>();
	}
}

WindowFrame::~WindowFrame()
{
	CEGUI::System::getSingleton().destroyGUIContext(*UIContext);
}

void WindowFrame::InjectInput()
{
	input.BeginNewFrame();

	//check if the UI is blocking all the input
	if (IsCapturingInput())
	{
		if (IsLevelValid())
			level->SetRespondToInput(false);
	}
	else
	{
		if (IsLevelValid())
			level->SetRespondToInput(true);
	}

	//ALWAYS update cursor position
	input.CursorPositionUpdate();
	UIContext->injectMousePosition((float)input.cursor_loc_.x, (float)input.cursor_loc_.y);

	while (SDL_PollEvent(&Event))								//get message from system
	{														//and
		if (Event.type == SDL_QUIT)							//store the message to input class
		{
			AddCommand(Command(PROGRAM, int(ProgramCommandType::EXP), "frame"));
		}
		else if (Event.type == SDL_KEYDOWN)
		{
			input.KeyDownEvent(Event.key.keysym.scancode);
			UIContext->injectKeyDown(input.SDL_CEGUI_keymap_[Event.key.keysym.sym]);
		}
		else if (Event.type == SDL_KEYUP)
		{
			input.KeyUpEvent(Event.key.keysym.scancode);
			UIContext->injectKeyUp(input.SDL_CEGUI_keymap_[Event.key.keysym.sym]);
		}
		else if (Event.type == SDL_MOUSEMOTION || Event.type == SDL_MOUSEBUTTONUP || Event.type == SDL_MOUSEBUTTONDOWN)
		{
			input.CursorUpdate(Event);
			if (Event.type == SDL_MOUSEBUTTONDOWN || Event.type == SDL_MOUSEBUTTONUP)
			{
				//handle button down event
				if (Event.button.button == SDL_BUTTON_LEFT && Event.type == SDL_MOUSEBUTTONDOWN)
				{
					UIContext->injectMouseButtonDown(CEGUI::LeftButton);
				}
				else if (Event.button.button == SDL_BUTTON_RIGHT && Event.type == SDL_MOUSEBUTTONDOWN)
				{
					UIContext->injectMouseButtonDown(CEGUI::RightButton);
				}
				//handle button up event
				else if (Event.button.button == SDL_BUTTON_LEFT && Event.type == SDL_MOUSEBUTTONUP)
				{
					UIContext->injectMouseButtonUp(CEGUI::LeftButton);
				}
				else if (Event.button.button == SDL_BUTTON_RIGHT && Event.type == SDL_MOUSEBUTTONUP)
				{
					UIContext->injectMouseButtonUp(CEGUI::RightButton);
				}
			}
		}
		else if (Event.type == SDL_MOUSEWHEEL)
		{
			input.WheelEvent(Event.wheel.y);
			UIContext->injectMouseWheelChange((float)Event.wheel.y);
		}
		else if (Event.type == SDL_TEXTINPUT)
		{
			try {
				CEGUI::utf32 cvt = utf::to_utf32(std::string(Event.text.text)).at(0);
				UIContext->injectChar(cvt);
				/*std::cout << "TextInput: " << Event.text.text << std::endl;*/
			}
			catch (std::exception& e) {}
		}
		else if (Event.type == SDL_TEXTEDITING)
		{
			try {
				//CEGUI::String cvt = utf::to_utf32((utf::string8)Event.edit.text);
				//UIContext->injectChar(cvt);
				/*std::cout << "TextEditing: " << Event.edit.text << std::endl*/;
			}
			catch (std::exception& e) {}
		}
	}
}

void WindowFrame::Draw()
{
	if (IsLevelValid())
	{
		level->Draw();
	}
	GUI::GetGUIClass()->GetRenderer()->beginRendering();

	UIContext->draw();
	for (auto &i : WidgetList)
	{
		i->Draw();
	}

	GUI::GetGUIClass()->GetRenderer()->endRendering();

	glDisable(GL_SCISSOR_TEST);
}
