#include "MessageBoxWidget.h"

MessageBoxWidget::MessageBoxWidget(const Exception & e, Functor OKhandler):
	IntegratedWidget("/IntegratedWidget/MessageBox1.layout"),
	OKHandler(OKhandler),
	IsFatal(e.is_fatal_)
{
	//set title
	Root->setText(e.message_queue_.front());

	//set message body
	MessageBody = Root->getChild(10000);
	//add every string in MessageQueue to the body
	//except for the first string (because it's the title )
	for (int i = 1; i < e.message_queue_.size(); i++)
		MessageBody->appendText(e.message_queue_[i]+ "\n");

	//subscribe to ok and close buttons
	Root->getChild(10001)->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(&MessageBoxWidget::OKClicked, this));
	Root->subscribeEvent(CEGUI::FrameWindow::EventCloseClicked, CEGUI::SubscriberSlot(&MessageBoxWidget::OKClicked, this));
}

MessageBoxWidget::MessageBoxWidget(Functor okHandler, const std::string & title, const std::string & body):
	IntegratedWidget("/IntegratedWidget/MessageBox2.layout"),
	OKHandler(okHandler),
	IsFatal(false)
{

	//set title
	Root->setText(title);

	//set message body
	MessageBody = Root->getChild(10000);
	MessageBody->appendText(body);

	//subscribe to ok and close buttons
	Root->getChild(10001)->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::SubscriberSlot(&MessageBoxWidget::OKClicked, this));
	Root->subscribeEvent(CEGUI::FrameWindow::EventCloseClicked, CEGUI::SubscriberSlot(&MessageBoxWidget::OKClicked, this));
}

bool MessageBoxWidget::OKClicked(const CEGUI::EventArgs & e)
{
	OKHandler((void*)&ID);
	return true;
}

bool MessageBoxWidget::IsCapturingInput()
{
	return false;
}
