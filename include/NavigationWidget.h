#pragma once
#include "IntegratedWidget.h"
#include <Command.h>

class NavigationWidget :
	public IntegratedWidget
{
private:
	CEGUI::PushButton* GameSettings;
	CEGUI::PushButton* ReturnToGame;
	CEGUI::PushButton* ExitToMenu;

	bool ExitToMenuClicked(const CEGUI::EventArgs & e);		//called when exit to menu button is clicked
	bool GameSettingsClicked(const CEGUI::EventArgs & e);	//called when settings button is clicked
	bool ReturnToGameClicked(const CEGUI::EventArgs & e);	//called when return to game button is clicked
public:
	std::string GetType();
	bool QueryIntAttribute(std::string AttName, int* valueHolder) { return false; }
	bool QueryFloatAttribute(std::string AttName, float* valueHolder) { return false; }
	bool QueryStringAttribute(std::string AttName, std::string* valueHolder) { return false; }
	void LinkValueToAttribute(std::string AttName, void* Value) {}
	bool SetAttribute(std::string AttName, void* Value) { return false; }
	void Update() {}
	virtual bool IsCapturingInput() { return false; }

	NavigationWidget();
	~NavigationWidget();
};

