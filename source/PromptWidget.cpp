#include "PromptWidget.h"



PromptWidget::PromptWidget(Functor f, const std::string & Title, const std::string & Message, const std::string & Opt1, const std::string & Opt2):
	IntegratedWidget("/IntegratedWidget/Prompt.layout"),
	func(f)
{
	Root->setText(Title);		//set title

	Root->getChild(10000)->setText(Message);		//set message

	Root->getChild(10001)->setText(Opt1);			//set opt1
	Root->getChild(10002)->setText(Opt2);			//set opt2

	//subscribe to all kinds of events
	Root->subscribeEvent(CEGUI::FrameWindow::EventCloseClicked, CEGUI::Event::Subscriber(&PromptWidget::CancelPicked, this));
	Root->getChild(10001)->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&PromptWidget::Option1Picked, this));
	Root->getChild(10002)->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&PromptWidget::Option2Picked, this));
}


PromptWidget::~PromptWidget()
{
}

std::string PromptWidget::GetType()
{
	return "PromptWidget";
}

bool PromptWidget::QueryIntAttribute(std::string AttName, int * valueHolder)
{
	return false;
}

bool PromptWidget::QueryFloatAttribute(std::string AttName, float * valueHolder)
{
	return false;
}

bool PromptWidget::QueryStringAttribute(std::string AttName, std::string * valueHolder)
{
	return false;
}

void PromptWidget::LinkValueToAttribute(std::string AttName, void * Value)
{
}

bool PromptWidget::SetAttribute(std::string AttName, void * Value)
{
	return false;
}

void PromptWidget::Update()
{
}

bool PromptWidget::IsCapturingInput()
{
	//the confirm exit window should always capture all inputs
	return true;
}

bool PromptWidget::Option1Picked(const CEGUI::EventArgs & e)
{
	std::string s(Root->getChild(10001)->getText().c_str());

	//call subscriber with option1's text
	func(&s);
	return true;
}

bool PromptWidget::Option2Picked(const CEGUI::EventArgs & e)
{
	std::string s(Root->getChild(10002)->getText().c_str());
	func(&s);
	return true;
}

bool PromptWidget::CancelPicked(const CEGUI::EventArgs & e)
{
	std::string s("Cancelled");
	func(&s);
	return true;
}
