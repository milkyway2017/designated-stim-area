#version 430 core
layout (location = 0) in vec2 aPos;
layout (location = 1) in vec4 aColor;
layout (location = 2) in vec2 aTexCoord;

out vec4 ourColor;
out vec2 TexCoord;

uniform mat4 CameraMatrix;
uniform vec2 FlashLightTopLeft;		//the top left coordinate of the flashlight texture
uniform int FlashlightRange;		//the side length of the area the flashlight can illuminate , named "cast_side_len_" in Cellphone.h

void main()
{
    gl_Position = vec4(CameraMatrix * vec4(aPos.xy,0.0 ,1.0));
    ourColor = aColor;
	
	//since this is flashlight shader, there is no TexCoord provided, we need to calculate it by...
	vec4 temp = vec4(aPos - FlashLightTopLeft, 0,0);
	
	temp.x = temp.x/FlashlightRange;
	temp.y = 1-(-temp.y/FlashlightRange);
    TexCoord = temp.xy;
}