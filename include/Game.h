#pragma once
#include <SDL.h>
#include "Audio.h"
#include "StartMenuFrame.h"
#include "GameFrame.h"
#include "SettingsFrame.h"

#define VAL 10

using namespace Fengine;
class Game
{
public:
	Game();
	~Game();

	//enters game loop
	void GameLoop();

	//initialize
	void Init();

private:
	void Draw();
	void Update(int TimeElapsed);
	bool IsDrawTime();						//compare the size of FRAME_TIME and ELAPSED_TIME_DRAW

	float CountFPS(Uint32 ElapsedTime);

	void CommandProc(std::vector<Command> CommandList);							//process all the commands from 
	void ConsoleDisplayHelp();
	bool EXP = 0;					//this would be true if exp command is passed in and game loop should be broken to exit program no matter what


	int LAST_DRAW_TIME;					//stores the time of last call of Draw()
	int FRAME_TIME = 17;				//the time between 2 Draw() calls,17 for 59 FPS
	const int max_update_time = 50;		//the maxinum amount of time between 2 updates
	int END_TIME;
	int ELAPSED_TIME;			//stores the time elapsed from last call of Update()
	int ELAPSED_TIME_DRAW;		//stores the time elapsed from last call of Draw()
	std::vector<Uint32> FPSVec;				//stores all time intervals for FPS counting

	Logger SystemLog;
	std::vector<Frame_ptr> FrameList;				//storess all the WindowFrames loaded
	//* means need to be added
	//0: StartMenuFrame
	//1: GameFrame
	//2: SettingsFrame
	//
	int ActiveFrame;							//the index of the frame that is active at the moment

	//change the active frame to a given index
	//returns 0 when invalid index is passed
	bool ChangeActiveFrame(int i);
	//change the active frame by frame's name
	//<name>: the name of the frame to change to, can be
	//-StartMenuFrame
	//-GameFrame
	//-SettingsFrame
	//returns 0 when invalid index is passed
	bool ChangeActiveFrame(const std::string & name);

	int LastFrame;								//records the last frame used before the current one, initialize to 0
};

