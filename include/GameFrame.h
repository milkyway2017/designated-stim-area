#pragma once
#include "WindowFrame.h"
class GameFrame :
	public WindowFrame
{
private:
	bool ExitFrame(const CEGUI::EventArgs& e);

	Widget_ptr* ConsolePtr;
public:
	///<systemlog>: the system's logger 
	GameFrame(Logger* systemlog);
	~GameFrame();

	std::vector<Command> CommandProc(std::vector<Command> commmands);
	virtual std::vector<Command> Update(Uint32 ElapsedTime);
	virtual void Draw();
	virtual bool IsCapturingInput();
};

