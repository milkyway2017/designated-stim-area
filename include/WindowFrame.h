#pragma once
#include "Level.h"
#include <utf/utf.h>
#include "ConsoleWidget.h"
#include "PromptWidget.h"
#include "NavigationWidget.h"
#include "MessageBoxWidget.h"
#include <Input.h>

using namespace Fengine;


class WindowFrame
{
protected:
	Level_ptr level;							//holds either a background or gameworld
	CEGUI::GUIContext* UIContext;			//holds all the GUI of this window frame
	std::vector<Command> CommandQueue;		//holds all the command that needs to be returned in Update(), is cleared at every beginning of InjectInput
	//void LoadSettingsLayout();
	CEGUI::Window* Root;					//root window of WindowFrame
	Logger* SystemLog;							//the pointer to SystemLog
	std::vector<Widget_ptr>	WidgetList;		//holds all IW owned by current WindowFrame

	/******************** WindowFrame Utilities, can be used by derived class********************************************/



	//toggle nav widget
	bool ToggleNavigationWidget(bool onoff);
	//add navigation widget
	void AddNavigationWidget();									




	//add a consle widget, do not show
	void AddConsoleWidget(bool withShadow);	
	//show the console
	void ToggleConsoleWidget(bool onoff);




	//add a messagebox
	unsigned int AddMessageBoxWidget(const std::string & title, const std::string & body, Functor f);
	//add a messagebox with exception
	unsigned int AddMessageBoxWidget(const Exception & e, Functor f);
	//toggle a messagebox, given its ID
	void ToggleMessageBoxWidget(unsigned int widgetID, bool onoff);
	//add a confirm exit widget with shadow
	void AddConfirmExitWidget(const std::string & Title = "Exit?", 
		const std::string & Message = "Do you really want to exit?", 
		const std::string & Opt1 = "Yes", 
		const std::string & Opt2 = "No",
		Functor callback = Functor());
	//reset the messagebox when OK is clicked
	void HandleExceptionOK(void * WidgetID);		



	//show the confirm widget
	void ToggleConfirmExitWidget(bool onoff);	
	//to show/not show <Shadow> and its child
	//callback for Confirm exit to decide
	virtual void HandleConfirmExit(void*) {};



	void ToggleShadow(bool onoff);										

	inline bool IsLevelValid()
	{
		return level.get() != nullptr;
	}
	/********************************************************************************************************************/


	virtual std::vector<Command> CommandProc();						//process commands in the command queue, return commands to Game class

private:
	Input input;
	SDL_Event Event;

	CEGUI::Window* Shadow;					//blocks all window-user interactions below the Shader StaticImage

	void HandleCopyNPaste();				//handles copy and paste base on the hotkeys pressed
	void ConsoleDisplayHelp();				//
	std::vector<Command> KeyInputHandler(Input & input);					//respond to key input
	
	//get an widget using ID
	IntegratedWidget* GetWidget(unsigned int WidgetID);

	//Remove a widget by its ID
	void RemoveWidget(unsigned int WidgetID);

	//remove a static widget by its type
	void RemoveStaticWidget(const std::string & Type);

	//get an widget using type, works for static widgets (typically widgets that exist only once in a frame,
	//e.g. NavigationWidget)
	inline IntegratedWidget* GetStaticWidget(const std::string & WidgetType) const
	{
		for (auto& i : WidgetList)
		{
			if (i->GetType() == WidgetType)
			{
				return i.get();
			}
		}
		return nullptr;
	}
public:
	WindowFrameType Type;

	WindowFrame(WindowFrameType type, Logger* log);
	virtual ~WindowFrame();

	//takes in all the input and informs CEGUI about it
	void InjectInput();

	virtual std::vector<Command> Update(Uint32 ElapsedTime);
	virtual void Draw();

	//if the frame is capturing keyboard input
	virtual bool IsCapturingInput();

	//for CEGUI
	bool OpenConfirmExit(const CEGUI::EventArgs & e);			//opens the confirm exit
	//for CEGUI
	bool OpenNavigation(const CEGUI::EventArgs & e);		//opens the navigation

	inline void AddCommand(Command c)										//add a command to the <CommandQueue>
	{
		CommandQueue.push_back(c);
	}
};


typedef std::unique_ptr<WindowFrame> Frame_ptr;
