#pragma once
#include <Map.h>
#include <Object.h>
#include <Input.h>
#include <nlohmann/json.hpp>
#include <StatusBar.h>
#include <thread>

using namespace Fengine;

/**
 * holds information about the file that may load
 */
struct MapInfo
{
	std::string map_file_name;			//!< path to load file from
	std::string loading_image;			//!< image to display while loading
};

class GameWorld
{
private:
	std::vector<std::unique_ptr<Map>> maps_;				//!< all the maps in this game world
	std::string gw_file_;									//!< file from which this game world reads its map
	std::vector<MapInfo> map_infos_;						//!< list of map files in json, which can be loaded at any time
	unsigned int entry_map_index_;
	std::vector<Command> CommandProc(std::vector<Command>);
	std::vector<Command> KeyInputHandler(Input & input);
	std::vector<Command> CommandQueue;						//!< a list of command that needs to be processed
	Logger* system_log_;


	/******************************
	 * status bars and UI overlay**
	 ******************************/
	StatusBar player_health_;
	StatusBar player_stamina_;
    Sprite player_img_;
	GLuint shader_program_;								// UI health bar shader_program_
    Rect4F player_img_src_rect_;
    Rect4F player_img_dest_rect_;
	Sprite loading_sprite_;								//!< background when loading map
	Sprite inventory_sprite_;
	std::vector<std::pair<Sprite*, Rect4F>> inventory_table_;  // table of Sprites and Rect4Fs for inventory

	
    /**
     * helper function, gets all the maps that are currently active
     * @return the vector containing maps
     */
    std::vector<Map*> GetActiveMaps();
    
    /**
     * load all the maps in map_infos_, which is loaded from gw_file_
     * @note this function may take up seconds to complete, therefore inside it will be a mini update-draw loop
     */
    void LoadMaps();

    /**
     * load the GameWorld
     */
	void Load();

	/**
	 * Updates Healthbar and inventory based on map
	 * @param map the Map
	 */
	void UpdateOverlayUI(Map* map);

	/**
	 * draw overlay UI
	 */
	void DrawOverlayUI();
public:
	inline void AddCommand(Command c)			//adds a command to he command queue
	{
		CommandQueue.push_back(c);
	}
	bool Active;										//if this is not active please dont update this gameworld
	std::vector<Command> Update(int ElapsedTime, Input & input);								//update
	void Draw();						//draws the current game world
	GameWorld(const std::string & path, Logger* systemlog, GLuint shader_program);		//pass in the SDL_Point on the screen where you want to put the game world
	
	GameWorld() {};
	~GameWorld();
};

