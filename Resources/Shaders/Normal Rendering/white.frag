#version 430 core
out vec4 FragColor;
  
in vec4 ourColor;
in vec2 TexCoord;

uniform sampler2D Texture;

void main()
{
    vec4 texColor = texture(Texture, TexCoord);
    FragColor = texColor;
    FragColor.rgb += ourColor.rgb;
    FragColor.a *= ourColor.a;
}