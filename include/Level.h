#pragma once
#include <string>
#include <Input.h>
#include "GameWorld.h"
#include <iostream>
#include <memory>
#include <GUI.h>
#include <Audio.h>

using namespace Fengine;

//Class that holds the core of a Level, contains either:
//a GameWorld
//a list of Sprite as background image
//or both
class Core
{
public:
	Core()
	{
		world.reset(nullptr);				//init to nullptr
	}

	~Core()
	{}

	//add an imgae as background
	void AddBackgroundImage(Sprite s);

	//set a gameworld
	void SetGameWorld(GameWorld* gw);

	bool HasGameWorld();

	std::unique_ptr<GameWorld> world;										//stores the gameworld of the level
	std::vector<Sprite> BackgroundList;									//stores all the background images of current level
};

class Level
{
private:
	Camera camera;
	Core* LevelCore;
	Logger* SystemLog;								//a pointer to the system log
	SDL_Rect Viewport;					//the viewport of this level
	GLuint ShaderProgram;				//the shader program used to render level
	std::vector<Command> CommandProc();										//process command in <CommandQueue>
	std::vector<Command> KeyInputHandler(Input & input);					//respond to key input
	std::vector<Command> CommandQueue;

	//makes not copyable
	Level(const Level& other) = delete;
	Level& operator=(const Level& other) = delete;

	void ConsoleDisplayHelp();

	bool Visible;

	bool Updatable;

	bool ShouldBeDeleted;			//set to 0, a value of 1 means this level should be deleted from the parent

	bool RespondToInput;			//is set to 0 when UI has the input focus
public:

	inline void AddCommand(Command c)										//add a command to the <CommandQueue>
	{
		CommandQueue.push_back(c);
	}
	std::string LevelName;

	//getters 
	inline bool GetVisible() { return Visible; }
	inline bool GetUpdatable() { return Updatable; }
	inline bool GetShouldBeDeleted() { return ShouldBeDeleted; }
	inline bool GetRespondToInput() { return RespondToInput; }

	//setters
	void SetVisible(bool onoff)
	{
		Visible = onoff;
	}
	void SetUpdatable(bool onoff)
	{
		Updatable = onoff;
	}
	void SetShouldBeDeleted(bool onoff)
	{
		ShouldBeDeleted = onoff;
	}
	void SetRespondToInput(bool onoff)
	{
		RespondToInput = onoff;
	}

	bool SuccessfullyCreated;
	bool IsPrimaryLevel;															//will be true if this level is primary
	//It: current level's position in Parent vector
	//Parent: the vector this level is in
	Level(std::string path, bool IsPrimary, std::string LevelName, Logger* systemlog);
	~Level();
	std::vector<Command> Update(int TimeElpased, Input & input);				//returns a sequence of commands back to main
	void Draw();												//draws the current level
	void ActivateLevel();														//activates the level by setting visibility and updatability
	void DeactivateLevel();													//deactivates the level by setting visibility and updatability
};

typedef std::unique_ptr<Level> Level_ptr;


inline void UpdateLevelList(std::vector<std::unique_ptr<Level>>& List)
{
	for (auto i = List.begin(); i != List.end();)
	{
		if (!(*i)->GetShouldBeDeleted())		//if the level should be deleted
		{
			i++;
			continue;
		}
		else
			//if not
		{
			i = List.erase(i);
			continue;
		}
	}
}