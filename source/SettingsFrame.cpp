#include "SettingsFrame.h"



SettingsFrame::SettingsFrame(Logger* log) :
	WindowFrame(SETTINGS_FRAME, log)
{
	try
	{
		level.reset(new Level("Resources/Levels/Settings/Level.json", 1, "settings", log));
		level->ActivateLevel();
	}
	catch (const Exception& e)
	{
		AddMessageBoxWidget(e, Functor((WindowFrame*)this, &SettingsFrame::HandleExceptionOK));
	}
	UIContext->setDefaultFont("DejaVuSans-14");
	UIContext->getMouseCursor().setDefaultImage("TaharezLook/MouseArrow");
	UIContext->setDefaultTooltipType("TaharezLook/Tooltip");

	//load layout
	Root = GUI::GetGUIClass()->LoadWindow("Settings.layout");
	UIContext->setRootWindow(Root);

	//subscribe to button events
	Root->getChild(10000)->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&SettingsFrame::ExitFrame, this));


	//add console and not show it
	AddConsoleWidget(false);
	ToggleConsoleWidget(0);

	//get combobox window 
	resolutionBox = (CEGUI::Combobox*)Root->getChild(10001);
	CEGUI::ListboxTextItem* item1 = new CEGUI::ListboxTextItem("1920x1080", 1);
	CEGUI::ListboxTextItem* item2 = new CEGUI::ListboxTextItem("1366x760", 1);
	CEGUI::ListboxTextItem* item3 = new CEGUI::ListboxTextItem("1600x900", 1);
	CEGUI::ListboxTextItem* item4 = new CEGUI::ListboxTextItem("1280x1024", 1);
	resolutionBox->addItem(item1);
	resolutionBox->addItem(item2);
	resolutionBox->addItem(item3);
	resolutionBox->addItem(item4);
	//subscribe to resolution combobox
	resolutionBox->subscribeEvent(CEGUI::Combobox::EventListSelectionAccepted, CEGUI::Event::Subscriber(&SettingsFrame::ResolutionChanged, this));

	ConsolePtr = &WidgetList.back();
}
std::vector<Command> SettingsFrame::CommandProc(std::vector<Command> commmands)
{
	return WindowFrame::CommandProc();
}

std::vector<Command> SettingsFrame::Update(Uint32 ElapsedTime)
{
	return WindowFrame::Update(ElapsedTime);
}

void SettingsFrame::Draw()
{
	WindowFrame::Draw();
}

bool SettingsFrame::IsCapturingInput()
{
	return WindowFrame::IsCapturingInput();			//the gameframe itself does not have any edit box so just check the widgets using WindowFrame's built in function
}

bool SettingsFrame::ExitFrame(const CEGUI::EventArgs& e)
{
	//add a command that tells the Game to use the last frame before the current frame
	CommandQueue.push_back(Command(PROGRAM, int(ProgramCommandType::CGF), "SettingsFrame", "(LastFrame)"));
	return true;
}

bool SettingsFrame::ResolutionChanged(const CEGUI::EventArgs & e)
{
	//changes the resolution
	std::string newRes = resolutionBox->getText().c_str();
	SDL_Point newResWH;
	//log it
	SystemLog->log_ += "new resolution selected: " + newRes + "\n";
	//identify the new resolution
	if (newRes == "1920x1080")
	{
		newResWH.x = 1920; newResWH.y = 1080;
	}
	else if (newRes == "1366x760")
	{
		newResWH.x = 1366; newResWH.y = 768;
	}
	else if (newRes == "1600x900")
	{
		newResWH.x = 1600; newResWH.y = 900;
	}
	else if (newRes == "1280x1024")
	{
		newResWH.x = 1280; newResWH.y = 1024;
	}
	//grab all CEGUI textures
	GUI::GetGUIClass()->GetRenderer()->grabTextures();

	//set new resolution using the Graphics class
	Graphics::GetInstance()->SetWindowSize(newResWH);

	//restore CEGUI textures and resize
	GUI::GetGUIClass()->GetRenderer()->restoreTextures();
	GUI::GetGUIClass()->GetRenderer()->setDisplaySize(CEGUI::Sizef(newResWH.x, newResWH.y));

	//we need to inform the rest of the program, with a Command to [game]
	CommandQueue.push_back(Command(PROGRAM, int(ProgramCommandType::NEW_RES), "SettingsFrame"));

	return true;
}

SettingsFrame::~SettingsFrame()
{
	CEGUI::WindowManager::getSingleton().destroyWindow(Root);
}