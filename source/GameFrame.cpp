#include "GameFrame.h"



GameFrame::GameFrame(Logger* log):
	WindowFrame(GAMEPLAY_FRAME, log)
{
	//load layout
	Root = GUI::GetGUIClass()->LoadWindow("Game.layout");
	UIContext->setRootWindow(Root);

	try
	{
		level.reset(new Level("Resources/Levels/GameWorld/Level.json", 1, "startmenu", log));
		level->ActivateLevel();
	}
	catch (const Exception& e)
	{
		AddMessageBoxWidget(e, Functor((WindowFrame*)this, &GameFrame::HandleExceptionOK));
	}

	UIContext->setDefaultFont("DejaVuSans-14");
	UIContext->getMouseCursor().setDefaultImage("TaharezLook/MouseArrow");
	UIContext->setDefaultTooltipType("TaharezLook/Tooltip");


	//subscribe to events of child windows
	Root->getChild(10000)->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&WindowFrame::OpenNavigation, static_cast<WindowFrame*>(this)));

	AddNavigationWidget();
	AddConsoleWidget(false);

	ToggleNavigationWidget(0);

	ConsolePtr = &WidgetList.back();
	(*ConsolePtr)->Toggle(false);
}

std::vector<Command> GameFrame::CommandProc(std::vector<Command> commmands)
{
	return WindowFrame::CommandProc();
}

std::vector<Command> GameFrame::Update(Uint32 ElapsedTime)
{
	return WindowFrame::Update(ElapsedTime);
}

void GameFrame::Draw()
{
	WindowFrame::Draw();
}

bool GameFrame::IsCapturingInput()
{
	return WindowFrame::IsCapturingInput();			//the gameframe itself does not have any edit box so just check the widgets using WindowFrame's built in function
}

bool GameFrame::ExitFrame(const CEGUI::EventArgs& e)
{
	//add a command that tells the Game to use 0th of FrameList(which is startmenuframe)
	CommandQueue.push_back(Command(PROGRAM, int(ProgramCommandType::CGF),"GameFrame", "(0)"));
	return true;
}

GameFrame::~GameFrame()
{
	CEGUI::WindowManager::getSingleton().destroyWindow(Root);
}


