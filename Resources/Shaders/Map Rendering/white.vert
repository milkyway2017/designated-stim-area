#version 430 core
layout (location = 0) in vec2 aPos;
layout (location = 1) in vec4 aColor;
layout (location = 2) in vec2 aTexCoord;

out vec4 ourColor;
out vec2 TexCoord;

uniform mat4 CameraMatrix;

void main()
{
    gl_Position = vec4(CameraMatrix * vec4(aPos.xy,0.0 ,1.0));
    ourColor = aColor;
    TexCoord = vec2(aTexCoord.x,-aTexCoord.y);
}