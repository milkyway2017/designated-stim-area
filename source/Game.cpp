#include "Game.h"
#include "Interpolation.h"

using namespace Fengine;
Game::Game()
{
}


Game::~Game()
{
}

void Game::CommandProc(std::vector<Command> CommandList)
{
	for (unsigned int i = 0; i < CommandList.size(); i++)
	{
		std::vector<std::string> ContentList;

		int cmdLevel = CommandList[i].command_level_;
		if (cmdLevel== NO_LEVEL)
		{
			CommandList[i].CannotResolve(&SystemLog);
			continue;
		}
		if (cmdLevel > PROGRAM)
		{	//push to the current ActiveFrame
			FrameList[ActiveFrame]->AddCommand(CommandList[i]);
			continue;
		}

		//now seperate the contents
		ContentList = CommandList[i].param_string_list_;

		switch ((ProgramCommandType)CommandList[i].command_type_)
		{
		case ProgramCommandType::EXP:
				EXP = true;
				CommandList[i].Resolve();			//resolve
				break;
		case ProgramCommandType::CGF:	//the command to change ActiveFrame
				try
				{
					ChangeActiveFrame(std::stoi(ContentList.at(0)));
					CommandList[i].Resolve();			//resolve
				}
				catch (...)
				{//if this kind of exception is thrown, we know that "LastFrame" is passed
					if (!ChangeActiveFrame(ContentList.at(0)))
					{
						CommandList[i].CannotResolve(&SystemLog);
					}
				}
				break;
		case ProgramCommandType::NEW_RES:
				for (auto &f : FrameList)
				{
					f->AddCommand(Command(LEVEL,(int)LevelCommandType::NEW_RES, CommandList[i].commander_));
					f->AddCommand(Command(MAP, (int)MapCommandType::NEW_RES, CommandList[i].commander_));
				}
				CommandList[i].Resolve();			//resolve
				break;
		default:
				CommandList[i].CannotResolve(&SystemLog);
		}

	}
}

bool Game::ChangeActiveFrame(int newframe)
{
	if (newframe > 2 || newframe <0)
	{
		return false;
	}
	LastFrame = ActiveFrame;				//record the last frame used before switching to new frame
	ActiveFrame = newframe;
	return true;
}

bool Game::ChangeActiveFrame(const std::string & name)
{
	if (name == "LastFrame")
		ChangeActiveFrame(LastFrame);
	else if (name == "StartMenuFrame")
		ChangeActiveFrame(0);
	else if (name == "GameFrame")
		ChangeActiveFrame(1);
	else if (name == "SettingsFrame")
		ChangeActiveFrame(2);
	else
	{
		return false;
	}
	return true;
}

void Game::GameLoop()
{

	int START_TIME = SDL_GetTicks();

	//setup threads//////////////////////////////////////////////////////
	while (!EXP)
	{
		FrameList[ActiveFrame]->InjectInput();
		END_TIME = SDL_GetTicks();
		ELAPSED_TIME = END_TIME - START_TIME;
		START_TIME = END_TIME;
#undef min
		Update(std::min(ELAPSED_TIME, max_update_time));
		if (IsDrawTime())
		{
			Draw();
		}
	}
}

void Game::Update(int TimeElapsed)
{
	CommandProc(FrameList[ActiveFrame]->Update(TimeElapsed));
	Interpolation::UpdatePool(TimeElapsed);
}

float Game::CountFPS(Uint32 ElapsedTime)
{
	static int Count = 0;			//stores the position in array--FPSVec
	if (Count == FPSVec.size())		//if FPS vec is not initialized yet
	{
		FPSVec.push_back(ElapsedTime);
	}
	else
	{
		FPSVec[Count] = ElapsedTime;
	}
	if (Count == VAL - 1)
	{
		Count = 0;
	}
	else
		Count++;

	int Sum = 0;			//sum up the vector
	for (auto i : FPSVec)
	{
		Sum += i;
	}

	return 1000 / ((float)Sum / (float)FPSVec.size());
}

bool Game::IsDrawTime()
{
	Uint32 CurrentTime = SDL_GetTicks();
	ELAPSED_TIME_DRAW += (CurrentTime - LAST_DRAW_TIME);


	if (ELAPSED_TIME_DRAW >= FRAME_TIME)
	{
		LAST_DRAW_TIME = CurrentTime;
		ELAPSED_TIME_DRAW = 0;
		return true;
	}
	return false;
}

void Game::Draw()
{
	GLErrorVal();
	Graphics::GetInstance()->BeginDrawing();
	GLErrorVal();
	FrameList[ActiveFrame]->Draw();
	GLErrorVal();
	Graphics::GetInstance()->EndDrawing();
	GLErrorVal();

}
void Game::Init()
{
	try
	{
		//load all kinds of shit
		Audio::GetAudioInstance();//initialize audio

		//set default paths (and initialize window)
		Graphics::GetInstance("MIT Admissions")->SetPaths("Resources/Images/","Resources/Fonts/");

		//import some colors
		Graphics::GetInstance()->LoadColor("Resources/Stats/colors.json");

		//initialize frames
		FrameList.emplace_back(new StartMenuFrame(&SystemLog));
		FrameList.emplace_back(new GameFrame(&SystemLog));
		FrameList.emplace_back(new SettingsFrame(&SystemLog));
		this->LastFrame = 0;
		this->ActiveFrame = 0;
	}
	catch(const Exception& e)
	{
		throw e;
	}
}
