#pragma once
#include "IntegratedWidget.h"
#include <Logger.h>
#include <utf/utf.h>

using namespace Fengine;

class ConsoleWidget :
	public IntegratedWidget
{
public:
	ConsoleWidget();
	~ConsoleWidget();

	virtual std::string GetType();
	virtual bool QueryIntAttribute(std::string AttName, int* valueHolder);
	virtual bool QueryFloatAttribute(std::string AttName, float* valueHolder);
	virtual bool QueryStringAttribute(std::string AttName, std::string* valueHolder);
	virtual void LinkValueToAttribute(std::string AttName, void* Value);
	virtual bool SetAttribute(std::string AttName, void* Value);
	//syncs statictext with log
	virtual void Update();
	virtual bool IsCapturingInput();

private:
	virtual void ProcessCommand(Command c);			//process one command and if the Command is not meant to be processed here add it to the CommandQueue	

	//add what's in the edit box to log, update StaticText field, clear edit Box
	bool SubmitTextBox(const CEGUI::EventArgs& e);

	//minimize the window when "x" button clicked
	bool Minimize(const CEGUI::EventArgs& e);

	//takes care when KeyDownEvent is detected, check...
	//ESC key to minimize-> call Minimize()
	bool KeyDown(const CEGUI::EventArgs& e);

	CEGUI::Window* StaticText;			//the widget that displays the log
	CEGUI::PushButton* SubmitButton;		//the "Submit" button
	CEGUI::Editbox* EditBox;				//the field where user can enter text and submit

	Logger* log;			//the logger this Console is set to take care of (sync when there is a change in UI)

};

