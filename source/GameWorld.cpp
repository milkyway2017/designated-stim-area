
#include <GameWorld.h>

#include "GameWorld.h"

using namespace Fengine;

std::vector<Command> GameWorld::CommandProc(std::vector<Command> CommandList)
{
	COMMANDPROCBEGIN
	if (i.command_level_ < GAMEWORLD)
	{
		ReturnList.push_back(i);
		continue;
	}
	else if (i.command_level_ > GAMEWORLD)
	{
		this->GetActiveMaps()[0]->AddCommand(i);
		continue;
	}
	else
	{
		i.CannotResolve(system_log_);
		continue;
	}

	//TODO: loadmap command...
	COMMANDPROCEND
}

std::vector<Command> GameWorld::KeyInputHandler(Input & input)
{
	std::vector<Command> ReturnList = std::vector<Command>();
	//respond to the input
	if (input.IsKeyPressed(SDL_SCANCODE_ESCAPE))
	{
		/*Object.Save();*/
		ReturnList.push_back(Command(PROGRAM, int(ProgramCommandType::EXP),"GameWorld"));
	}
	return ReturnList;				//nothing to be returned
}

std::vector<Command> GameWorld::Update(int ElapsedTime, Input & input)
{
	//lazy initialization
	if(maps_.empty())
	{
		LoadMaps();
	}

	std::vector<Command> return_list = std::vector<Command>();		//Commands to return

	auto active_maps_ = GetActiveMaps();
	return_list = CommandProc(this->CommandQueue);

	//update all active maps
	for(auto & i : active_maps_)
	{
		auto temp_list = i->Update(input, ElapsedTime);
		return_list.insert(return_list.end(), temp_list.begin(), temp_list.end());
	}

	//display player stats
	if(!active_maps_.empty())
	{
		UpdateOverlayUI(active_maps_[0]);
	}

	return return_list;
}

void GameWorld::UpdateOverlayUI(Map* map)
{
	auto player = (Player *) map->GetPlayer();
	if (player)
	{
		//get values to set
		float health_ratio = (float) player->GetHealth() / (float) player->GetMaxHealth();
		std::string health = std::to_string(player->GetHealth())
							 + "/"
							 + std::to_string(player->GetMaxHealth());

		float stamina_ratio = (float) player->GetStamina() / (float) player->GetMaxStamina();
		std::string stamina = std::to_string(player->GetStamina())
							  + "/"
							  + std::to_string(player->GetMaxStamina());

		//set
		player_health_.SetValue(health_ratio);
		player_health_.SetText(health);
		player_stamina_.SetValue(stamina_ratio);
		player_stamina_.SetText(stamina);
	} else
	{
		player_health_.SetValue(0);
		player_health_.SetText("Dead");
		player_stamina_.SetValue(0);
		player_stamina_.SetText("");
	}

	auto item_list = player->GetItemList();

	for (unsigned int i = 0; i < item_list.size(); i++)
	{
		if(item_list[i])
			inventory_table_[i].first = item_list[i]->GetIcon();
	}

}

void GameWorld::Draw()
{
	auto active_maps = GetActiveMaps();

	//draw all the active maps
	for(auto & i : active_maps)
	{
		i->Draw();
	}


	if(active_maps.empty())
	{
		loading_sprite_.Draw(shader_program_);
	}
	//if there is at least a map, draw map stuff
	else
	{
		//DrawOverlayUI();
	}
}

void GameWorld::DrawOverlayUI()
{
	//healthbar stuff
	player_health_.Draw(shader_program_);
	player_stamina_.Draw(shader_program_);
	player_img_.Draw(shader_program_, (SDL_Rect)player_img_src_rect_, player_img_dest_rect_);

	//inventory
	for(auto& pair : inventory_table_)
	{
		//draw icon if there is one
		if(pair.first != nullptr)
		{
			//draw
			pair.first->Draw(shader_program_, SDL_Rect(0, 0, 64, 64), pair.second);
			//clear
			pair.first = nullptr;
		}
		//draw inventory slot (frame)
		else
		{
			inventory_sprite_.Draw(shader_program_);
		}
	}
}

GameWorld::GameWorld(const std::string & path, Logger* systemlog, GLuint shader_program) :
        system_log_(systemlog),
        Active(1),
        shader_program_(shader_program),
        gw_file_(path),
		entry_map_index_(0)
{
	//load gameworld, (does not actually load any maps)
	Load();

	// Initialization of inventory
	inventory_sprite_.Init("Resources/Images/Inventory.png", Rect4F(0, 0, 64, 64));

	// Initialization of player's image
	player_img_.Init("Resources/Images/PlayerImg.png", Rect4F());
	SDL_Point player_img__wh = Graphics::GetInstance()->GetTextureWH(player_img_.sprite_sheet_);
	player_img_src_rect_ = Rect4F(0, 0, (float) player_img__wh.x, (float) player_img__wh.y);
	player_img_dest_rect_ = Rect4F(20, 50, (float) player_img__wh.y, (float) player_img__wh.y);

    // Initialization of the health bar
    LabelStyle status_style;
	status_style.color = Graphics::GetInstance()->GetColor("white");
	status_style.fade_in_time = 0;
	status_style.fade_out_time = 0;

	//initialize status bars
    player_health_ = StatusBar("Resources/Images/Health.png", "Resources/Images/BarFrame.png", Point2F(95, 97), status_style);
	player_stamina_ = StatusBar("Resources/Images/Stamina.png", "Resources/Images/BarFrame.png", Point2F(95, 70), status_style);

	//initialize loading sprite
	loading_sprite_.Init();

	//set position of inventory
	inventory_table_.resize(6, std::pair<Sprite*, Rect4F>(nullptr, Rect4F()));
	for (unsigned int i = 0; i < 6; i++)
	{
		inventory_table_[i].second.x = 840 + (i%3)*65;
		inventory_table_[i].second.y = 650 + int(i/3)*65;
		inventory_table_[i].second.w = 65;
		inventory_table_[i].second.h = 65;
	}
}

GameWorld::~GameWorld()
{
}

std::vector<Map *> GameWorld::GetActiveMaps()
{
	std::vector<Map*> return_list;

	for(auto & i : maps_)
	{
		if(i->is_active_)
			return_list.push_back(i.get());
	}

	return return_list;
}

void GameWorld::Load()
{
	using json = nlohmann::json;

	//read file as JSON
	json gw_json;
	if(!ReadJson(gw_json, gw_file_).empty())
	{
		//if file fails to load, throw a fatal exception
		throw Exception({"GameWorld Loading Error","failed to load GameWorld from", "path: " + gw_file_}, true);
	}

	try
	{
		gw_json = gw_json["gameworld"];
	}
	catch(const std::exception & e)
	{
		throw Exception({"GameWorld Loading Error","\"gameworld\" element must exist in ", "path: " + gw_file_}, true);
	}

	for(unsigned int i = 0; i < gw_json["maps"].size(); i++)
	{
		MapInfo map_info;
		map_info.loading_image = gw_json["maps"][i]["loading_image"];
		map_info.map_file_name = gw_json["maps"][i]["path"];

		map_infos_.push_back(map_info);

		if(gw_json["maps"][i].count("is_entry"))
		{
			entry_map_index_ = i;		//set entry map to this map
		}
	}
}

void GameWorld::LoadMaps()
{

	LabelStyle label_style;
	label_style.color = Graphics::GetInstance()->GetColor("white");;
	label_style.font = Graphics::GetInstance()->GetFont("manaspc.ttf", 30);
	StatusBar progress_bar = StatusBar("Resources/Images/Health.png", "Resources/Images/BarFrame.png", Point2F(348.f, 545.f), label_style);
	Label progress_text = Label("", Point2F(348.f, 645.f), label_style);
	progress_bar.SetScale(3.0f);

	for(unsigned int i = 0; i < map_infos_.size(); i++)
	{
		//"update"
		loading_sprite_.Init(map_infos_[i].loading_image, {0,0,1280,720});
		loading_sprite_.SetTransparency(.5f);

		//update progress
		progress_text.SetText("Loading Map " + std::to_string(i + 1));
		progress_bar.SetValue((float)i/map_infos_.size());

		//"draw"
		Graphics::GetInstance()->BeginDrawing();
		loading_sprite_.Draw(shader_program_);
		progress_bar.Draw(shader_program_);
		progress_text.Draw(shader_program_);
		Graphics::GetInstance()->EndDrawing();

		//actually load
		try
		{
			maps_.emplace_back(new Map(map_infos_[i].map_file_name, system_log_, i == entry_map_index_));
		}
		catch(const nlohmann::json::exception & e)
		{
			this->system_log_->AddLine(e.what() + std::string(", exception id: ") + std::to_string(e.id));
			this->AddCommand(Command(PROGRAM, (int)ProgramCommandType::CGF, "GameWorld", "(StartMenuFrame)"));
			break;
		}
		catch(const std::exception & e)
		{
			this->system_log_->AddLine(e.what());
			this->AddCommand(Command(PROGRAM, (int)ProgramCommandType::CGF,"GameWorld", "(StartMenuFrame)"));
			break;
		}
	}
}
