#pragma once
#include <IntegratedWidget.h>
#include <Exception.h>
#include <GUI.h>
#include <Functor.h>

using namespace Fengine;

class MessageBoxWidget:
	public IntegratedWidget
{
public:
	//intented for exception handling
	//e: the exception that needs to be displayed
	//okHandler: a functor that handles when "OK" is pressed
	//				looks something like this:
	//				void HandleOK(void * WidgetID);
	//				where WidgetID is the void* form of the 
	//				ID of the MessageBoxWidget
	MessageBoxWidget(const Exception & e, Functor okHandler);


	//intended for general notification use
	//okHandler: a functor that handles when "OK" is pressed
	//				looks something like this:
	//				void HandleOK(void * WidgetID);
	//				where WidgetID is the void* form of the 
	//				ID of the MessageBoxWidget
	//title: the title of the messagebox
	//body: the body message of the messagebox
	MessageBoxWidget(Functor okHandler, const std::string & title, const std::string & body);

	virtual std::string GetType() { return "MessageBoxWidget";  };

	/********************data query functions*********************************/
	//all blank
	virtual bool QueryIntAttribute(std::string AttName, int* valueHolder) { return false; }
	virtual bool QueryFloatAttribute(std::string AttName, float* valueHolder) { return false; }
	virtual bool QueryStringAttribute(std::string AttName, std::string* valueHolder) { return false; }
	virtual void LinkValueToAttribute(std::string AttName, void* Value) {}
	virtual bool SetAttribute(std::string AttName, void* Value) { return false; }
	/*************************************************************************/

	virtual bool IsCapturingInput();
	virtual void Update() {}

	bool IsFatal;					//if this message (exception) is fatal, if true the window frame may toggle on shadow when showing this messagebox

	~MessageBoxWidget() {}
private:
	//messagebox's handler of the "OK" button click
	//which in turn calls OKHandler
	bool OKClicked(const CEGUI::EventArgs & e);
	Functor OKHandler;
	
	CEGUI::Window* MessageBody;				//contains the text of the message body
};