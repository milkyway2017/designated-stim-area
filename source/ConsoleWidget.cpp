#include "ConsoleWidget.h"



ConsoleWidget::ConsoleWidget() :
	IntegratedWidget("/IntegratedWidget/Console.layout")
{

	EditBox = static_cast<CEGUI::Editbox*>(Root->getChild("Editbox"));
	SubmitButton = static_cast<CEGUI::PushButton*>(Root->getChild("Button"));
	StaticText = Root->getChild("StaticText");

	SubmitButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ConsoleWidget::SubmitTextBox, this));
	Root->subscribeEvent(CEGUI::FrameWindow::EventCloseClicked, CEGUI::Event::Subscriber(&ConsoleWidget::Minimize, this));
	Root->subscribeEvent(CEGUI::Window::EventKeyDown, CEGUI::Event::Subscriber(&ConsoleWidget::KeyDown, this));
	EditBox->subscribeEvent(CEGUI::Editbox::EventTextAccepted, CEGUI::Event::Subscriber(&ConsoleWidget::SubmitTextBox, this));
}

std::string ConsoleWidget::GetType()
{
	return "ConsoleWidget";
}

bool ConsoleWidget::QueryIntAttribute(std::string AttName, int * valueHolder)
{
	//ConsoleWidget does not hold any int Attribute
	return false;
}

bool ConsoleWidget::QueryFloatAttribute(std::string AttName, float * valueHolder)
{
	//ConsoleWidget does not hold any int Attribute
	return false;
}

bool ConsoleWidget::QueryStringAttribute(std::string AttName, std::string * valueHolder)
{
	return false;
}

void ConsoleWidget::LinkValueToAttribute(std::string AttName, void * Value)
{
	if (AttName == "Logger")
	{
		log = (Logger*)Value;
	}
}

bool ConsoleWidget::SetAttribute(std::string AttName, void * Value)
{
	return false;
}

void ConsoleWidget::Update()
{
	StaticText->setText(this->log->log_);
}

bool ConsoleWidget::IsCapturingInput()
{
	return (Root->isCapturedByThis() || Root->isCapturedByChild() || EditBox->hasInputFocus());
}

void ConsoleWidget::ProcessCommand(Command c)
{
	CommandQueue.push_back(c);
}

std::string AddSlashes(const std::string & s)
{
	std::string ReturnString = s;
	int currentPos = 0;
	do
	{
		currentPos = ReturnString.find('[', currentPos);
		if ((currentPos != std::string::npos))
		{
			ReturnString.insert(currentPos, "\\");
			currentPos += 3;
		}
		else
		{
			break;
		}
	} while (true);
	return ReturnString;
}

bool ConsoleWidget::SubmitTextBox(const CEGUI::EventArgs & e)
{
	std::string text = EditBox->getText().c_str(); //get content
	if (text.size() == 0) return true;			//ignore if there is no text
	EditBox->setText("");		//clear editbox

	this->log->log_ += "[colour='FF05F0E4'][font='DejaVuSans-12']>[colour='FFFFFFFF']";
	this->log->log_ += AddSlashes(text);
	this->log->log_ += '\n';

	try
	{
		ProcessCommand(Command(text));		//process the command (add the command to the 'system')
	}
	catch (const std::exception& e)
	{
		this->log->log_ += "[font='DejaVuSans-12'][colour='FFFF0000']";		//set colour
		this->log->log_ += e.what();					//add exception message to console
		this->log->log_ += '\n';
	}
	//update the text displayed by console widget
	Update();

	return true;
}

bool ConsoleWidget::Minimize(const CEGUI::EventArgs & e)
{
	static_cast<CEGUI::FrameWindow*>(Root)->toggleRollup();
	return true;
}

bool ConsoleWidget::KeyDown(const CEGUI::EventArgs & e)
{
	auto k = static_cast<CEGUI::KeyEventArgs*>(const_cast<CEGUI::EventArgs*>(&e));

	switch (k->scancode)
	{
	case CEGUI::Key::Escape:
		Minimize(CEGUI::EventArgs());
		break;
	}

	return true;
}

ConsoleWidget::~ConsoleWidget()
{
}
