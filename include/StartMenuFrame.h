#pragma once
#include "WindowFrame.h"
class StartMenuFrame :
	public WindowFrame
{
private:
	bool OpenGame(const CEGUI::EventArgs& e);
	bool OpenSettings(const CEGUI::EventArgs& e);
	void HandleConsoleInput(void* decisionstring) {}
	void HandleConfirmExit(void* decisionstring);
public:
	///<systemlog>: the system's logger 
	StartMenuFrame(Logger* systemlog);
	~StartMenuFrame();

	std::vector<Command> CommandProc(std::vector<Command> commmands);
	virtual std::vector<Command> Update(Uint32 ElapsedTime);
	virtual void Draw();
	virtual bool IsCapturingInput();
};

