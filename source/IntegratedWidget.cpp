#include "IntegratedWidget.h"

unsigned int IntegratedWidget::AvailableWidgetID = 0;

IntegratedWidget::IntegratedWidget(const std::string &path):
	ID(AvailableWidgetID)
{
	Root = GUI::GetGUIClass()->LoadWindow(path);
	//increment AvailableWidgetID
	AvailableWidgetID++;
}

void IntegratedWidget::Draw()
{
	/*
	any custom rendering happens here
	*/
	if (Child != nullptr)
	{
		Child->Draw();
	}
}

IntegratedWidget::~IntegratedWidget()
{
	CEGUI::WindowManager::getSingleton().destroyWindow(Root);
}

std::vector<Command> IntegratedWidget::ExtractCommand()		//return all commands in queue and clear queue
{
	if (CommandQueue.size() != 0)
	{
		auto ReturnList = CommandQueue;
		CommandQueue.clear();
		return ReturnList;
	}
	return std::vector<Command>();
}