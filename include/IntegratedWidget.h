#pragma once
#include <string>
#include <CEGUI/CEGUI.h>
#include <memory>
#include <GUI.h>
#include <Command.h>

//An IntegratedWidget is an object that has a specific purpose and is evoked by a WindowFrame

using namespace Fengine;
class IntegratedWidget;
typedef std::unique_ptr<IntegratedWidget> Widget_ptr;
class IntegratedWidget
{
public:
	//<path>: the path of the .layout file should be under the Resources/GUI/layouts/IntegratedWidget
	IntegratedWidget(const std::string &path);

	//gets the type of the IntegratedWidget in the form of string
	virtual std::string GetType() = 0;

	//Get an int attribute holded by the widget
	//returns true if succeeded
	//		   false if attribute <AttName> does not exist
	virtual bool QueryIntAttribute(std::string AttName, int* valueHolder) = 0;
	//Get an float attribute holded by the widget
	//returns true if succeeded
	//		   false if attribute <AttName> does not exist
	virtual bool QueryFloatAttribute(std::string AttName, float* valueHolder) = 0;
	//Get an string attribute holded by the widget
	//returns true if succeeded
	//		   false if attribute <AttName> does not exist
	virtual bool QueryStringAttribute(std::string AttName, std::string* valueHolder) = 0;

	//tells the IW to bind an attribute to a value(any value depending on the IW)
	//that way once the attribute is modified, the IW can change the bound value (volume bar, console update...)
	//NOTE: this is only a one way sync, meaning any change from the <Value> will not be detected by IW
	virtual void LinkValueToAttribute(std::string AttName, void* Value) = 0;

	//set an attribute to <Value>
	//returns true if No Error
	//returns false if Error occurs
	virtual bool SetAttribute(std::string AttName, void* Value) = 0;

	//attach the <Root> to parent frame's window
	inline void AttachToWindow(CEGUI::Window* Parent)
	{
		this->Parent = Parent;
		Parent->addChild(Root);
	}

	//detach the <Root> from parent frame's window
	inline void DetachFromWindow()
	{
		Parent->removeChild(Root->getID());
		Parent = nullptr;
	}

	inline bool IsAttached()
	{
		return Parent != nullptr;
	}

	inline unsigned int GetID() const
	{
		return ID;
	}

	//toggles the widget on or off
	inline void Toggle(bool onoff)
	{
		if (onoff)
		{
			//never hurts to set the parent visible
			Parent->setVisible(true);
			Parent->activate();
		}
		Root->setVisible(onoff);
	}
	
	virtual void Update() = 0;
	void Draw();
	virtual ~IntegratedWidget();

	CEGUI::Window* Parent;			//pointer to the Parent window that <Root> is attached to
	CEGUI::Window* Root;		//the window that holds the IW's UI components

	std::vector<Command> ExtractCommand();		//return all commands in queue and clear queue

	//if the widget is capturing keyboard input
	virtual bool IsCapturingInput() = 0;
protected:
	const unsigned int ID;

	Widget_ptr Child;					//a child slot

	std::vector<Command> CommandQueue;		//list of commands


	virtual void ProcessCommand(Command c) {};			//process one command and if the Command is not meant to be processed here just add it to the CommandQueue	
private:
	static unsigned int AvailableWidgetID;			//contains the newest available ID
};