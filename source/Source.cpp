#include "Game.h"

int main(int argc, char* argv[])
{
	Game game;
	try
	{
		game.Init();
	}
	catch(const Exception& e)
	{
		std::string Messages;
		for(auto i: e.message_queue_)
		{
			Messages.append(i+"\n");
		}
		CommandDumpLog::GetInstance()->logger_.AddLine("fail to initialize game: " + Messages);
		CommandDumpLog::GetInstance()->~CommandDumpLog();
		return 1;
	}

	game.GameLoop();
	std::cout << "end of program" << std::endl;
	//export log
	CommandDumpLog::GetInstance()->~CommandDumpLog();
	return 0;
}