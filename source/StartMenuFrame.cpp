#include "StartMenuFrame.h"


StartMenuFrame::StartMenuFrame(Logger* systemlog) :
	WindowFrame(STARTMENU_FRAME, systemlog)
{
	try 
	{
		level.reset(new Level("Resources/Levels/StartMenu/Level.json", 0, "startmenu", systemlog));
		level->ActivateLevel();
	}
	catch (const Exception& e)
	{
		AddMessageBoxWidget(e, Functor((WindowFrame*)this, &StartMenuFrame::HandleExceptionOK));
	}

	UIContext->setDefaultFont("DejaVuSans-14");
	UIContext->getMouseCursor().setDefaultImage("TaharezLook/MouseArrow");
	UIContext->setDefaultTooltipType("TaharezLook/Tooltip");

	//load layout
	Root = GUI::GetGUIClass()->LoadWindow("StartMenu.layout");
	UIContext->setRootWindow(Root);

	//subscribe to events of child windows
		//subscribe to "play game" button
	Root->getChild(10000)->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&StartMenuFrame::OpenGame, this));

	//subscribe to "exit game" button
	Root->getChild(10001)->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&WindowFrame::OpenConfirmExit, static_cast<WindowFrame*>(this)));
	//subscribe to "settings" button
	Root->getChild(10002)->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&StartMenuFrame::OpenSettings, this));
	AddConfirmExitWidget();
	AddConsoleWidget(false);

	ToggleConsoleWidget(0);
	ToggleShadow(0);
	ToggleConfirmExitWidget(0);
}

bool StartMenuFrame::OpenGame(const CEGUI::EventArgs& e)
{
	//add a command that tells the Game to use 1st of FrameList(which is GameFrame)
	CommandQueue.push_back(Command(PROGRAM, int(ProgramCommandType::CGF), "StartMenuFrame","(1)"));
	return true;
}

bool StartMenuFrame::OpenSettings(const CEGUI::EventArgs& e)
{
	CommandQueue.push_back(Command(PROGRAM, int(ProgramCommandType::CGF), "StartMenuFrame", "(2)"));
	return true;
}

void StartMenuFrame::HandleConfirmExit(void* decisionstring)
{
	auto str = static_cast<std::string*>(decisionstring);
	if (*str == "Yes")		//if the player confirms, e
	{
		CommandQueue.push_back(Command(PROGRAM, int(ProgramCommandType::EXP), "StartMenuFrame"));
	}
	else								//otherwise close prompt & continue...
	{
		ToggleConfirmExitWidget(0);
		ToggleShadow(0);
	}
}

StartMenuFrame::~StartMenuFrame()
{
	CEGUI::WindowManager::getSingleton().destroyWindow(Root);
}

std::vector<Command> StartMenuFrame::CommandProc(std::vector<Command> commmands)
{
	return WindowFrame::CommandProc();
}

std::vector<Command> StartMenuFrame::Update(Uint32 ElapsedTime)
{
	return WindowFrame::Update(ElapsedTime);
}

void StartMenuFrame::Draw()
{
	WindowFrame::Draw();
}

bool StartMenuFrame::IsCapturingInput()
{
	return WindowFrame::IsCapturingInput();			//the StartMenuFrame itself does not have any edit box so just check the widgets using WindowFrame's built in function
}
